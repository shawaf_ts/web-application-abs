# Web Application for Appointment Booking System.

## 1. General Information
This application is developed using react. This application serves as a portal for bussinesses to register and manager appointments

## 2. Features
* Register Bussiness
* Accept/Reject Appointments
* Define time interval available for appointments (On Signup)
* Define working days in week (On Signup)
* Define appointment duration (On Signup)

## 3. Pages

### 3.1 Login
Provides interface for login

[Screenshot](https://ibb.co/nwHmD07)

### 3.2 Signup
Provides interface for registering. While registering, user can give details regarding place, working days in a week, office hours, office breaks (customers won't be able to book appointment in this window), typical duration of an appointment.

[Screenshot 1](https://ibb.co/4TRmV0q) [Screenshot 2](https://ibb.co/pLB1L2X)

### 3.3 Appointments
This page displays all the accepted appointments for this seller.


[Screenshot](https://ibb.co/cb8rVzr)

### 3.4 Requests
This page displays all the pending appointment requests for this particular seller. When a buyer requests for an appointment through **Appointment Booking App**, seller will be able to view and **Accept/Reject** the requests through this page.


[Screenshot](https://ibb.co/ThxXGJH)