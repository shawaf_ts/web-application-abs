import React, { Component } from "react";
import { ToastContainer } from "react-toastify";
import { Route, Redirect, Switch } from "react-router-dom";
import Appointments from "./component/pages/appointments";
import Login from "./component/pages/login";
import ProtectedRoute from "./component/common/protectedRoute";
import PendingRequests from "./component/pages/pendingRequests";
import Logout from "./component/pages/logout";
import NavBar from "./component/navBar";
import auth from "./services/authService";
import Signup from "./component/pages/signup";
import NotFound from "./component/pages/notFound";
import "react-toastify/dist/ReactToastify.css";
import "./App.css";

class App extends Component {
  state = {};
  componentDidMount() {
    const user = auth.getCurrentUser();
    this.setState({ user });
  }
  render() {
    const { user } = this.state;
    return (
      <React.Fragment>
        <ToastContainer />
        <main className="container">
          {user && <NavBar user={user} />}
          <Switch>
            <Route path="/login" exact component={Login} />
            <Route path="/logout" exact component={Logout} />
            <Route path="/signup" exact component={Signup} />
            <Route path="/not-found" component={NotFound} />
            <ProtectedRoute
              path="/appointments"
              exact
              component={Appointments}
            />
            <ProtectedRoute
              path="/pending_requests"
              exact
              component={PendingRequests}
            />
            <Redirect from="/" exact to="/appointments" />
            <Redirect to="/not-found" />
          </Switch>
        </main>
      </React.Fragment>
    );
  }
}

export default App;
