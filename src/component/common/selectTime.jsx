import React from "react";
import { convertTimeToReadableFormat } from "../../helpers/util";
const SelectTime = ({
  name,
  label,
  options,
  error,
  errorMessage = "",
  ...rest
}) => {
  return (
    <div className="form-group">
      <label htmlFor={name}>{label}</label>
      <select name={name} id={name} {...rest} className="form-control">
        <option value="" />
        {options.map(option => (
          <option key={option} value={option}>
            {convertTimeToReadableFormat(option)}
          </option>
        ))}
      </select>
      {error &&
        (errorMessage ? (
          <div className="alert alert-danger">{errorMessage}</div>
        ) : (
          <div className="alert alert-danger">{error}</div>
        ))}
    </div>
  );
};

export default SelectTime;
