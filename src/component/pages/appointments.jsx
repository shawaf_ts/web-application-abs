import React, { Component } from "react";

import {
  convertDateToReadableFormat,
  convertTimeToReadableFormat,
  capitaliseFirstLetter
} from "../../helpers/util";
import { getAcceptedAppointments } from "../../services/appointmentService";

class Appointments extends Component {
  state = {
    appointments: []
  };

  async componentDidMount() {
    const { data: appointments } = await getAcceptedAppointments();
    this.setState({ appointments });
  }

  render() {
    const { appointments } = this.state;
    if (appointments.length === 0)
      return (
        <div className="jumbotron" style={{ marginTop: "10%" }}>
          <h5 className="display-5" style={{ textAlign: "center" }}>
            No Appointments Found
          </h5>
        </div>
      );
    return (
      <table className="table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Date</th>
            <th>Time</th>
          </tr>
        </thead>
        <tbody>
          {this.state.appointments.map(appointment => (
            <tr key={appointment._id}>
              <td>{capitaliseFirstLetter(appointment.buyer.name)}</td>
              <td>
                {convertDateToReadableFormat(appointment.appointmentDate)}
              </td>
              <td>
                {convertTimeToReadableFormat(appointment.appointmentStartTime)}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }
}

export default Appointments;
