import React from "react";
import { Redirect } from "react-router-dom";
import Joi from "joi-browser";
import Form from "../common/form";
import auth from "../../services/authService";
import { toast } from "react-toastify";

class Login extends Form {
  state = {
    data: { username: "", password: "" },
    errors: {}
  };

  schema = {
    username: Joi.string()
      .required()
      .label("Username"),
    password: Joi.string()
      .required()
      .label("Password")
  };

  doSubmit = async () => {
    try {
      const { data } = this.state;
      await auth.login(data.username.toLowerCase(), data.password);

      const { state } = this.props.location;
      window.location = state ? state.from.pathname : "/";
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        const errors = { ...this.state.errors };
        errors.username = ex.response.data;
        this.setState({ errors });
      }
    }
  };

  render() {
    if (auth.getCurrentUser())
      return (
        <React.Fragment>
          {toast.error("User Already Logged in, Please signout")}
          <Redirect to="/" />
        </React.Fragment>
      );

    return (
      <div style={{ padding: "10%" }}>
        <h1>Login</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("username", "Username")}
          {this.renderInput("password", "Password", "password")}
          {this.renderButton("Login")}
        </form>

        <div style={{ paddingTop: "20px" }}>
          {/* <h6 style={{ fontWeight: "normal" }}>New User?</h6> */}
          <button
            onClick={() => {
              this.props.history.push("/signup");
            }}
            className="btn btn-primary"
          >
            Register
          </button>
        </div>
      </div>
    );
  }
}

export default Login;
