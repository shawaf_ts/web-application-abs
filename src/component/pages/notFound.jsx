import React from "react";

const NotFound = () => {
  return (
    <div className="jumbotron" style={{ marginTop: "10%" }}>
      <h5 className="display-5" style={{ textAlign: "center" }}>
        Page Not Found
      </h5>
    </div>
  );
};

export default NotFound;
