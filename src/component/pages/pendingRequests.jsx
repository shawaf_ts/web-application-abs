import React, { Component } from "react";
import { toast } from "react-toastify";
import {
  convertDateToReadableFormat,
  convertTimeToReadableFormat,
  capitaliseFirstLetter
} from "../../helpers/util";
import {
  getPendingAppointments,
  updateStatus
} from "../../services/appointmentService";

class PendingRequests extends Component {
  state = {
    appointments: []
  };

  handleAccept = async appointment => {
    const statusOfTheBooking = "ACCEPTED";
    this.updateStatusOfBooking(appointment, statusOfTheBooking);
  };

  handleReject = async appointment => {
    const statusOfTheBooking = "REJECTED";
    this.updateStatusOfBooking(appointment, statusOfTheBooking);
  };

  updateStatusOfBooking = async (appointment, statusOfTheBooking) => {
    const originalAppointments = this.state.appointments;
    const appointments = originalAppointments.filter(
      a => a._id !== appointment._id
    );
    this.setState({ appointments });
    try {
      await updateStatus(appointment._id, {
        statusOfTheBooking: statusOfTheBooking
      });
    } catch (error) {
      toast.error("Something went wrong");
      this.setState({ appointment: originalAppointments });
    }
  };

  async componentDidMount() {
    const { data: appointments } = await getPendingAppointments();
    this.setState({ appointments });
  }

  render() {
    const { appointments } = this.state;
    if (appointments.length === 0)
      return (
        <div className="jumbotron" style={{ marginTop: "10%" }}>
          <h5 className="display-5" style={{ textAlign: "center" }}>
            No Pending Appointment Requests
          </h5>
        </div>
      );
    return (
      <table className="table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Date</th>
            <th>Time</th>
            <th>Accept</th>
            <th>Reject</th>
          </tr>
        </thead>
        <tbody>
          {this.state.appointments.map(appointment => (
            <tr key={appointment._id}>
              <td>{capitaliseFirstLetter(appointment.buyer.name)}</td>
              <td>
                {convertDateToReadableFormat(appointment.appointmentDate)}
              </td>
              <td>
                {convertTimeToReadableFormat(appointment.appointmentStartTime)}
              </td>
              <td>
                <button
                  onClick={() => {
                    this.handleAccept(appointment);
                  }}
                  className="btn btn-success btn-sm"
                >
                  Accept
                </button>
              </td>
              <td>
                <button
                  onClick={() => {
                    this.handleReject(appointment);
                  }}
                  className="btn btn-danger btn-sm"
                >
                  Reject
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }
}

export default PendingRequests;
