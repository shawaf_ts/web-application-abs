import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { toast } from "react-toastify";
import Input from "../common/input";
import Select from "../common/select";
import SelectTime from "../common/selectTime";
import { generateTimeList, validateSeller } from "../../helpers/util";
import { signup, getCurrentUser } from "../../services/authService";

class Signup extends Component {
  state = {
    data: {
      name: "",
      email: "",
      password: "",
      place: "",
      workingDays: [],
      startTime: "480",
      endTime: "960",
      breakStartTime: "720",
      breakEndTime: "780",
      duration: 30
    },
    errors: {},
    isMonday: true,
    isTuesday: true,
    isWednesday: true,
    isThursday: false,
    isFriday: false,
    isSaturday: true,
    isSunday: true
  };
  //   workingDays = ["1", "2", "3", "4", "5", "6", "7"];
  duration = [30, 60];
  timeList = generateTimeList();
  handleSubmit = async e => {
    // console.log("Handle Submit");
    e.preventDefault();
    let stateData = this.state;
    //console.log(stateData);
    stateData = this.addWorkingDays(stateData);
    this.setState({ data: stateData.data });
    // console.log(stateData.data.workingDays);
    const { error } = validateSeller(stateData.data);
    if (error) {
      const errors = {};
      console.log("Error path:", error.details[0].path);
      console.log("Error message:", error.details[0].message);
      errors[error.details[0].path] = error.details[0].message;
      this.setState({ errors });
      return;
    } else {
      this.setState({ errors: {} });
    }
    await this.doSubmit(this.state.data);
    // this.doSubmit();
  };

  doSubmit = async data => {
    console.log("From do Submit:", data);
    try {
      await signup(data);
      window.location = "/";
    } catch (error) {
      if (error.response && error.response.status === 409) {
        toast.error("User already registered");
      } else {
        toast.error("Something went wrong");
      }
    }
  };

  addWorkingDays = stateData => {
    stateData.data.workingDays = [];
    if (stateData.isMonday) {
      stateData.data.workingDays.push("1");
    }
    if (stateData.isTuesday) {
      stateData.data.workingDays.push("2");
    }
    if (stateData.isWednesday) {
      stateData.data.workingDays.push("3");
    }
    if (stateData.isThursday) {
      stateData.data.workingDays.push("4");
    }
    if (stateData.isFriday) {
      stateData.data.workingDays.push("5");
    }
    if (stateData.isSaturday) {
      stateData.data.workingDays.push("6");
    }
    if (stateData.isSunday) {
      stateData.data.workingDays.push("7");
    }
    return stateData;
  };

  handleChange = ({ currentTarget: input }) => {
    const data = { ...this.state.data };
    data[input.name] = input.value;
    this.setState({ data });
  };

  renderButton = label => {
    return <button className="btn btn-primary">{label}</button>;
  };

  renderInput = (name, label, type = "text") => {
    const { data, errors } = this.state;
    return (
      <Input
        name={name}
        label={label}
        type={type}
        value={data[name]}
        onChange={this.handleChange}
        error={errors[name]}
      />
    );
  };

  renderSelect = (name, label, options) => {
    const { data, errors } = this.state;
    return (
      <Select
        name={name}
        label={label}
        options={options}
        value={data[name]}
        onChange={this.handleChange}
        error={errors[name]}
      />
    );
  };

  renderSelectTime = (name, label, options, errorMessage = "") => {
    const { data, errors } = this.state;
    return (
      <SelectTime
        name={name}
        label={label}
        options={options}
        value={data[name]}
        onChange={this.handleChange}
        error={errors[name]}
        errorMessage={errorMessage}
      />
    );
  };

  toggleChangeMonday = () => {
    console.log("Monday");
    this.setState(prevState => ({
      isMonday: !prevState.isMonday
    }));
  };

  toggleChangeTuesday = () => {
    this.setState(prevState => ({
      isTuesday: !prevState.isTuesday
    }));
  };
  toggleChangeWednesday = () => {
    this.setState(prevState => ({
      isWednesday: !prevState.isWednesday
    }));
  };
  toggleChangeThursday = () => {
    this.setState(prevState => ({
      isThursday: !prevState.isThursday
    }));
  };
  toggleChangeFriday = () => {
    this.setState(prevState => ({
      isFriday: !prevState.isFriday
    }));
  };
  toggleChangeSaturday = () => {
    this.setState(prevState => ({
      isSaturday: !prevState.isSaturday
    }));
  };
  toggleChangeSunday = () => {
    this.setState(prevState => ({
      isSunday: !prevState.isSunday
    }));
  };
  render() {
    if (getCurrentUser())
      return (
        <React.Fragment>
          {toast.error("User Already Logged in, Please signout")}
          <Redirect to="/" />
        </React.Fragment>
      );
    const { errors } = this.state;
    return (
      <div className="py-5 px-5">
        <h1>Signup</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("name", "Name")}
          {this.renderInput("email", "Email")}
          {this.renderInput("password", "Password", "password")}
          {this.renderInput("place", "Place")}
          <p>Select Working Days</p>
          {errors.workingDays && (
            <div className="alert alert-danger">
              Select atleast one working day
            </div>
          )}
          <div className="form-check">
            <label className="form-check-label">
              <input
                type="checkbox"
                checked={this.state.isMonday}
                onChange={this.toggleChangeMonday}
                className="form-check-input"
              />
              Monday
            </label>
          </div>
          <div className="form-check">
            <label className="form-check-label">
              <input
                type="checkbox"
                checked={this.state.isTuesday}
                onChange={this.toggleChangeTuesday}
                className="form-check-input"
              />
              Tuesday
            </label>
          </div>
          <div className="form-check">
            <label className="form-check-label">
              <input
                type="checkbox"
                checked={this.state.isWednesday}
                onChange={this.toggleChangeWednesday}
                className="form-check-input"
              />
              Wednesday
            </label>
          </div>
          <div className="form-check">
            <label className="form-check-label">
              <input
                type="checkbox"
                checked={this.state.isThursday}
                onChange={this.toggleChangeThursday}
                className="form-check-input"
              />
              Thursday
            </label>
          </div>
          <div className="form-check">
            <label className="form-check-label">
              <input
                type="checkbox"
                checked={this.state.isFriday}
                onChange={this.toggleChangeFriday}
                className="form-check-input"
              />
              Friday
            </label>
          </div>
          <div className="form-check">
            <label className="form-check-label">
              <input
                type="checkbox"
                checked={this.state.isSaturday}
                onChange={this.toggleChangeSaturday}
                className="form-check-input"
              />
              Saturday
            </label>
          </div>
          <div className="form-check" style={{ paddingBottom: "20px" }}>
            <label className="form-check-label">
              <input
                type="checkbox"
                checked={this.state.isSunday}
                onChange={this.toggleChangeSunday}
                className="form-check-input"
              />
              Sunday
            </label>
          </div>
          {this.renderSelectTime(
            "startTime",
            "Office Opening Time",
            this.timeList,
            "Office Opening time should be less than closing time"
          )}
          {this.renderSelectTime(
            "endTime",
            "Office closing time",
            this.timeList
          )}
          {this.renderSelectTime(
            "breakStartTime",
            "Office break starts at (Lunch)",
            this.timeList,
            "This value must be before break ends value and it must be between opening and closing hoursr"
          )}
          {this.renderSelectTime(
            "breakEndTime",
            "Office break ends at",
            this.timeList,
            "This value must be less than office closing time"
          )}
          {this.renderSelect("duration", "Duration", this.duration)}

          {this.renderButton("Submit")}
        </form>
      </div>
    );
  }
}

export default Signup;
