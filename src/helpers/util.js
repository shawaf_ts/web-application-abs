import moment from "moment";
import Joi from "joi-browser";

const dayOfTheWeek = ["1", "2", "3", "4", "5", "6", "7"];
export const convertTimeToReadableFormat = number => {
  let hours = Math.floor(number / 60);
  let minutes = number % 60;
  let period = "AM";
  if (hours >= 12) {
    if (hours > 12) {
      hours = hours - 12;
    }
    period = "PM";
  }
  if (minutes < 10) {
    minutes = `0${minutes}`;
  }
  return `${hours}:${minutes} ${period}`;
};

export const convertDateToReadableFormat = dateText => {
  try {
    dateText = dateText.substr(0, dateText.indexOf("T"));
    let dateString =
      moment(dateText).date() +
      " " +
      moment(moment(dateText).month() + 1, "M").format("MMMM") +
      " " +
      moment(dateText).year();
    return dateString;
  } catch (error) {
    return dateText;
  }
};

export const capitaliseFirstLetter = string => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

export default {
  convertDateToReadableFormat,
  convertTimeToReadableFormat
};

export const generateTimeList = () => {
  let timeArray = [];
  const maxTime = 1440;
  for (let i = 0; i <= maxTime; i += 30) {
    timeArray.push(i);
  }
  return timeArray;
};

export const validateSeller = seller => {
  const schema = {
    name: Joi.string()
      .min(2)
      .max(50)
      .required(),
    email: Joi.string()
      .min(5)
      .max(50)
      .required()
      .email(),
    password: Joi.string()
      .min(6)
      .max(255)
      .required(),
    place: Joi.string()
      .min(3)
      .max(255)
      .required(),
    workingDays: Joi.array().items(
      Joi.string()
        .max(1)
        .valid(dayOfTheWeek)
        .required()
    ),
    endTime: Joi.number()
      .integer()
      .max(1440)
      .required(),
    startTime: Joi.number()
      .integer()
      .max(1440)
      .less(Joi.ref("endTime"))
      .required(),
    breakEndTime: Joi.number()
      .integer()
      .max(1440)
      .less(Joi.ref("endTime"))
      .required(),
    breakStartTime: Joi.number()
      .integer()
      .max(1440)
      .less(Joi.ref("breakEndTime"))
      .greater(Joi.ref("startTime"))
      .less(Joi.ref("endTime"))
      .required(),
    duration: Joi.number()
      .integer()
      .max(240)
      .min(30)
      .required()
  };
  return Joi.validate(seller, schema);
};
