import http from "./httpService";
import { apiUrl } from "../config.json";

const apiEndpoint = apiUrl + "/appointments";

export function getPendingAppointments() {
  return http.get(apiEndpoint + "/seller_pending");
}

export function updateStatus(id, data) {
  return http.put(apiEndpoint + "/" + id, data);
}

export function getAcceptedAppointments() {
  return http.get(apiEndpoint + "/seller_accepted");
}
