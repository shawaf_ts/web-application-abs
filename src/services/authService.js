import jwtDecode from "jwt-decode";
import http from "./httpService";
import { apiUrl } from "../config.json";

const apiEndpoint = apiUrl + "/sellers/signin";
const apiEndpointSignup = apiUrl + "/sellers/signup";
const tokenKey = "token";

http.setJwt(getJwt());

export async function login(email, password) {
  const { data: jwt } = await http.post(apiEndpoint, { email, password });
  localStorage.setItem(tokenKey, jwt);
}

export async function signup(data) {
  const response = await http.post(apiEndpointSignup, {
    email: data.email.toLowerCase(),
    password: data.password,
    name: data.name,
    place: data.place,
    workingDays: data.workingDays,
    startTime: data.startTime,
    endTime: data.endTime,
    breakStartTime: data.breakStartTime,
    breakEndTime: data.breakEndTime,
    duration: data.duration
  });
  console.log("sign up response:", response.headers["x-auth-token"]);
  loginWithJwt(response.headers["x-auth-token"]);
}

export function getCurrentUser() {
  try {
    const jwt = localStorage.getItem(tokenKey);
    return jwtDecode(jwt);
  } catch (ex) {
    return null;
  }
}

export function loginWithJwt(jwt) {
  localStorage.setItem(tokenKey, jwt);
}

export function getJwt() {
  return localStorage.getItem(tokenKey);
}

export function logout() {
  localStorage.removeItem(tokenKey);
}

export default {
  login,
  signup,
  loginWithJwt,
  logout,
  getCurrentUser,
  getJwt
};
